import os
import subprocess
import platform

def install_jdk():
    system = platform.system().lower()
    if system == 'linux':
        url = 'https://github.com/SAP/SapMachine/releases/download/sapmachine-11.0.24/sapmachine-jdk-11.0.24_linux-x64_bin.tar.gz'
    elif system == 'darwin':  # macOS
        url = 'https://download.oracle.com/java/11/latest/GPL/openjdk-11_macos-x64_bin.tar.gz'
    else:
        raise RuntimeError(f'Unsupported system: {system}')

    # Baixa o JDK
    jdk_filename = url.split('/')[-1]
    os.system(f'wget {url}')
    
    # Extrai o JDK
    os.system(f'tar -zxvf {jdk_filename}')
    
    # Configura as variáveis de ambiente
    java_home = os.path.abspath(os.path.splitext(jdk_filename)[0])
    os.environ['JAVA_HOME'] = java_home
    #os.environ['PATH'] += os.pathsep + os.path.join(java_home, 'bin')

    # Verifica a instalação
    #result = subprocess.run(['java', '-version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #if result.returncode == 0:
    #    print('JDK instalado com sucesso!')
    #else:
    #    print('Erro ao instalar o JDK.')

install_jdk()
