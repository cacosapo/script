import os
import subprocess
import shutil
import requests
import zipfile
import urllib3
urllib3.disable_warnings(category=urllib3.exceptions.InsecureRequestWarning)


###
#
# Global Variables
#
###
project_name='sapcommerce_project'
project_dir = os.path.expanduser(f"~/workspace/{project_name}")
repository_url='repositorio'

sap_commerce_core_zip='CXCOMCL221100U_25-70007431.ZIP'
sap_commerce_telco_zip='CXTELPACK230200U_6-80008253.ZIP'
sap_commerce_requirements_folder = os.path.expanduser(f"~/OneDrive - Telefonica/HY")
sap_machine_17_url='https://github.com/SAP/SapMachine/releases/download/sapmachine-23/sapmachine-jdk-23_macos-aarch64_bin.tar.gz'
sap_machine_17_destination = os.path.expanduser(f"~/workspace/{project_name}/sapmachine-jdk-23_macos-aarch64_bin.tar.gz")

###
#
# Function Definition
#
##
def create_project_dir(project_dir):
   os.makedirs(project_dir, exist_ok=True)
   print(f"Diretorio {project_dir} criado.")

def clone_repository(project_dir,repository_url):
   os.chdir(project_dir)
   subprocess.check_call(["git", "clone", repository_url])
   print("Clone do projeto finalizado.")

def copy_sap_commerce_requirements():
   os.chdir(project_dir)
   shutil.copy(os.path.join(sap_commerce_requirements_folder,sap_commerce_core_zip), project_dir)
   print(f"Arquivo {sap_commerce_core_zip} copiado para {project_dir}")
   shutil.copy(os.path.join(sap_commerce_requirements_folder,sap_commerce_telco_zip), project_dir)
   print(f"Arquivo {sap_commerce_telco_zip} copiado para {project_dir}")

def unzip_sap_commerce_requirements():
   os.chdir(project_dir)
   with zipfile.ZipFile(sap_commerce_core_zip) as file:
      file.extractall()
      print(f"Unzip arquivo {sap_commerce_core_zip}")
   with zipfile.ZipFile(sap_commerce_telco_zip) as file:
      file.extractall()
      print(f"Unzip arquivo {sap_commerce_telco_zip}")

def clean_requirements():
   os.chdir(project_dir)
   os.remove(sap_commerce_core_zip)
   print(f"Arquivo {sap_commerce_core_zip} removido")
   os.remove(sap_commerce_telco_zip)
   print(f"Arquivo {sap_commerce_telco_zip} removido")

def build_project():
   print(os.path.join(project_dir,'hybris','bin','platform'))
   os.chdir(os.path.join(project_dir,'hybris','bin','platform'))
   subprocess.call("source setantenv.sh && ant clean all -Dinput.template=develop", shell=True)


def download_file(url, destination):
   try:
       response = requests.get(url)
       response.raise_for_status()
       with open(destination, 'wb') as file:
           file.write(response.content)
       print(f"Arquivo baixado com sucesso: {destination}")
   except requests.exceptions.RequestException as e:
       print(f"Erro ao baixar o arquivo: {e}")

if __name__ == '__main__':
   create_project_dir(project_dir)
   clone_repository(project_dir, repository_url)
   copy_sap_commerce_requirements()
   unzip_sap_commerce_requirements()
   download_file(sap_machine_17_url,sap_machine_17_destination)
   clean_requirements()
   build_project()
   print("Setup concluído.")