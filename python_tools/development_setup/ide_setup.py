import os
import subprocess
import platform

def install_intellij():
    system = platform.system().lower()
    if system == 'linux':
        url = 'https://download-cdn.jetbrains.com/idea/ideaIC-2024.1.4.tar.gz'
    elif system == 'darwin':  # macOS
        url = 'https://download.jetbrains.com/idea/ideaIC-2021.1.3.dmg'
    else:
        raise RuntimeError(f'Unsupported system: {system}')

    # Baixa o IntelliJ IDEA
    intellij_filename = url.split('/')[-1]
    os.system(f'wget {url}')
    
    if system == 'linux':
        # Extrai o IntelliJ IDEA
        os.system(f'tar -zxvf {intellij_filename}')
    elif system == 'darwin':  # macOS
        # Monta o arquivo dmg
        os.system(f'hdiutil attach {intellij_filename}')
        # Copia o IntelliJ IDEA para a pasta de aplicativos
        os.system(f'cp -R /Volumes/IntelliJ\ IDEA\ */IntelliJ\ IDEA\ *.app /Applications/')
        # Desmonta o arquivo dmg
        os.system(f'hdiutil detach /Volumes/IntelliJ\ IDEA\ *')

    print('IntelliJ IDEA instalado com sucesso!')

install_intellij()