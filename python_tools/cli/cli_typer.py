import typer
from prettytable import PrettyTable
import subprocess
from scripts.scripts_portalapi import *
import pytest
import os
import shutil
from dotenv import load_dotenv, dotenv_values
# loading variables from .env file
load_dotenv()

test_host_default       =os.getenv("test_host_default")
test_basesiteid_default =os.getenv("test_basesiteid_default")
test_catalogid_default  =os.getenv("test_catalogid_default")
test_catalogversion_default=os.getenv("test_catalogversion_default")
test_folderpath_default =os.getenv("test_folderpath_default")
test_reportfile_default =os.getenv("test_reportfile_default")

podman_sap_commerce_compose_file = os.getenv("podman_sap_commerce_compose_file")

app = typer.Typer()
portalapi_app = typer.Typer()
app.add_typer(portalapi_app, name="portalapi")
pytest_app = typer.Typer()
app.add_typer(pytest_app, name="test")
dev_app = typer.Typer()
app.add_typer(dev_app, name="dev")
podman_app = typer.Typer()
app.add_typer(podman_app, name="podman_cli")


####################################################################################################################################################################
#
# Portal API Commands Section
#
####################################################################################################################################################################

@dev_app.command()
def list_workspace_project():
    """Backup dos workspace locais para cloud"""
    path_workspace = os.path.join(os.environ['HOME'], 'workspace')
    for item in os.listdir(path_workspace):
        typer.echo(item)
    typer.echo("Backup finalizado com sucesso")

####################################################################################################################################################################
#
# Portal API Commands Section
#
####################################################################################################################################################################
@portalapi_app.command("deploys")
def deploys():
    """Lista deployments Portal Api"""
    df = get_deploys()
    table = PrettyTable()
    table.field_names = df.columns.tolist()
    
    for index, row in df.iterrows():
        table.add_row(row.tolist())
    
    print(table)

@portalapi_app.command()
def builds():
    """Lista Builds Portal Portal"""
    df = get_builds()
    table = PrettyTable()
    table.field_names = df.columns.tolist()
    
    for index, row in df.iterrows():
        table.add_row(row.tolist())
    print(table)

@pytest_app.command()
def test_env(
    host:str = typer.Option(default=test_host_default),
    basesiteid:str = typer.Option(default=test_basesiteid_default),
    catalogid:str = typer.Option(default=test_catalogid_default),
    catalogversion:str = typer.Option(default=test_catalogversion_default),
    folderpath:str = typer.Option(default=test_folderpath_default), 
    reportfile:str = typer.Option(default=test_reportfile_default)):
    
    #pytest.main([folderpath, f"--html={reportfile}", "--myparam=paramet"])
    pytest.main([folderpath,
                 f"--host={host}",
                 f"--basesiteid={basesiteid}",
                 f"--catalogid={catalogid}",
                 f"--catalogversion={catalogversion}",
                 f"--html={reportfile}"
                 ])

####################################################################################################################################################################
#
# Poman Commands Section
#
####################################################################################################################################################################
@podman_app.command()
def start_sap_commerce():
    """Start SAP Commerce Compose."""
    try:
        result = subprocess.run(f"podman-compose -f {podman_sap_commerce_compose_file} up -d", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def status_sap_commerce():
    """Status SAP Commerce Compose."""
    try:
        result = subprocess.run(f"podman-compose -f {podman_sap_commerce_compose_file} ps", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def stop_sap_commerce():
    """Stop SAP Commerce Compose."""
    try:
        result = subprocess.run(f"podman-compose -f {podman_sap_commerce_compose_file} down", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def log_sap_commerce():
    """Logs SAP Commerce Compose."""
    print(f"podman-compose -f {podman_sap_commerce_compose_file} logs")
    try:
        result = subprocess.run(f"podman-compose -f {podman_sap_commerce_compose_file} logs", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def pull_base_images():
    """Logs SAP Commerce Compose."""
    try:
        result = subprocess.run("podman pull --arch=amd64 sapmachine:17-jdk-ubuntu", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def prune_images():
    """Prune Images."""
    try:
        result = subprocess.run("podman image prune -a --force", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)

@podman_app.command()
def build_images():
    """Logs SAP Commerce Compose."""
    print(f"podman-compose -f {podman_sap_commerce_compose_file} build")
    try:
        result = subprocess.run("podman pull --arch=amd64 sapmachine:17-jdk-ubuntu", shell=True, check=True, text=True, capture_output=True)
        typer.echo(f"Command executed successfully:\n{result.stdout}")
    except subprocess.CalledProcessError as e:
        typer.echo(f"Error executing command:\n{e.stderr}", err=True)


if __name__ == "__main__":
    app()