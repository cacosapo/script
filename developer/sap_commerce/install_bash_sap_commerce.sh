#!/bin/bash

SAPCOMMERCE_FOLDER=$HOME/workspace/sapcommerceteste

SAPCOM_CORE=CXCOMCL221100U_25-70007431.ZIP
SAPCOM_TELCO=CXTELPACK230200U_6-80008253.ZIP

ZIP_SAPCOMMERCE_CORE="<zippath>/$SAPCOM_CORE"
ZIP_SAPCOMMERCE_TELCO="<zippath>$SAPCOM_TELCO"

AZURE_REPOSITORY=<repository path>

echo "Criando pasta $SAPCOMMERCE_FOLDER"
mkdir -p $SAPCOMMERCE_FOLDER
cd $SAPCOMMERCE_FOLDER

echo "Clonando repositorio $AZURE_REPOSITORY"
git clone $AZURE_REPOSITORY . > /dev/null 

echo "Copiando core $ZIP_SAPCOMMERCE_CORE"
cp "$ZIP_SAPCOMMERCE_CORE" "$SAPCOMMERCE_FOLDER/." 
echo "Copiando telco $ZIP_SAPCOMMERCE_TELCO"
cp "$ZIP_SAPCOMMERCE_TELCO" $SAPCOMMERCE_FOLDER/.

echo "Descompactando core $SAPCOM_CORE"
unzip $SAPCOM_CORE > /dev/null
echo "Desompactando telco $SAPCOM_TELCO"
unzip -o $SAPCOM_TELCO > /dev/null

echo "Rename config file"
mv $SAPCOMMERCE_FOLDER/hybris/config $SAPCOMMERCE_FOLDER/hybris/config_old

echo "Build Limpo"
cd $SAPCOMMERCE_FOLDER/hybris/bin/platform
source setantenv.sh
ant clean all -Dinput.template=develop > /dev/null

echo "Build com codigo custom"
git reset --hard HEAD > /dev/null
ant clean all