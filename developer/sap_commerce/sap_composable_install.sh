# https://help.sap.com/docs/SAP_COMMERCE_COMPOSABLE_STOREFRONT/cfcf687ce2544bba9799aa6c8314ecd0/e38d45609de04412920a7fc9c13d41e3.html#loio51f36d6883b44151beb341b560d09cdc
# https://help.sap.com/docs/SAP_COMMERCE_COMPOSABLE_STOREFRONT/cfcf687ce2544bba9799aa6c8314ecd0/5de67850bd8d487181fef9c9ba59a31d.html


# Log into your S-user account at the following web address: https://ui.repositories.cloud.sap/www/webapp/users/ 
# In the root of your Angular application, create an .npmrc file with the following content: 
@spartacus:registry=https://73554900100900004337.npmsrv.base.repositories.cloud.sap/
//73554900100900004337.npmsrv.base.repositories.cloud.sap/:_auth=<npmcredentialsfromrbsc>
always-auth=true

# Using the Angular CLI, generate a new Angular application with the following command: 
ng new mystore --style=scss --routing=false --standalone=false

# You can add composable storefront core libraries and features to your Angular project by running the following command from your project root:
ng add @spartacus/schematics@latest

# The following is an example that generates an application that is ready to be used with the electronics storefront, that sets the baseUrl and the baseSite, and that also enables server-side rendering:
ng add @spartacus/schematics@latest --base-url https://composable-storefront-demo.eastus.cloudapp.azure.com:543/ --base-site=electronics-spa --ssr


###
#
# Default Setup SAP Commerce Composable
#
##

asdf list all nodejs
asdf list nodejs
asdf install nodejs 20.9.0
asdf global nodejs 20.9.0

npm install npm@10.2.4 -g
npm install -g @angular/cli@17

npm uninstall -g @angular/cli@17
npm cache clean --force

