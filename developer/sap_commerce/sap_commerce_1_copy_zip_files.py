import os
import shutil
import requests

###
#
# Global Variables
#
###
sap_commerce_requirements_folder = os.path.expanduser(f"~/Downloads")

project_dir='.'
sap_commerce_core_zip='CXCOMCL221100U_25-70007431.ZIP'
sap_commerce_telco_zip='CXTELPACK230200U_6-80008253.ZIP'

sap_machine_17_url='https://github.com/SAP/SapMachine/releases/download/sapmachine-23/sapmachine-jdk-23_macos-aarch64_bin.tar.gz'
sap_machine_17_destination = "./sapmachine-jdk-23_macos-aarch64_bin.tar.gz"

###
#
# Function Definition
#
##

def copy_sap_commerce_requirements():   
   shutil.copy(os.path.join(sap_commerce_requirements_folder,sap_commerce_core_zip), project_dir)
   print(f"Arquivo {sap_commerce_core_zip} copiado para {project_dir}")
   shutil.copy(os.path.join(sap_commerce_requirements_folder,sap_commerce_telco_zip), project_dir)
   print(f"Arquivo {sap_commerce_telco_zip} copiado para {project_dir}")

def download_file(url, destination):
   try:
       response = requests.get(url)
       response.raise_for_status()
       with open(destination, 'wb') as file:
           file.write(response.content)
       print(f"Arquivo baixado com sucesso: {destination}")
   except requests.exceptions.RequestException as e:
       print(f"Erro ao baixar o arquivo: {e}")

if __name__ == '__main__':
   copy_sap_commerce_requirements()
   download_file(sap_machine_17_url,sap_machine_17_destination)
   print("Copia de arquivos concluído.")