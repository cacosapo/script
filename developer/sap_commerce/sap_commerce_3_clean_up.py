import os

###
#
# Global Variables
#
###

sap_commerce_core_zip='CXCOMCL221100U_25-70007431.ZIP'
sap_commerce_telco_zip='CXTELPACK230200U_6-80008253.ZIP'
sap_machine_17_destination = "./sapmachine-jdk-23_macos-aarch64_bin.tar.gz"

###
#
# Function Definition
#
##
def clean_requirements():
   os.remove(sap_commerce_core_zip)
   print(f"Arquivo {sap_commerce_core_zip} removido")
   os.remove(sap_commerce_telco_zip)
   print(f"Arquivo {sap_commerce_telco_zip} removido")
   os.remove(sap_machine_17_destination)
   print(f"Arquivo {sap_machine_17_destination} removido")

if __name__ == '__main__':
   clean_requirements()
   print("Clean up concluído.")