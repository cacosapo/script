VBoxManage startvm node-01
VBoxManage guestcontrol "node-01" --username vagrant --password vagrant copyto $HOME/.ssh/id_ed25519.pub /home/vagrant
VBoxManage guestcontrol "node-02" --username vagrant --password vagrant copyto $HOME/.ssh/id_ed25519.pub /home/vagrant
VBoxManage guestcontrol "node-03" --username vagrant --password vagrant copyto $HOME/.ssh/id_ed25519.pub /home/vagrant
VBoxManage guestcontrol "node-04" --username vagrant --password vagrant copyto $HOME/.ssh/id_ed25519.pub /home/vagrant
VBoxManage import ~/Downloads/bkp/kali-linux-2022.2-virtualbox-amd64.ova --vsys 0 --eula accept --vmname teste

VBoxManage --nologo guestcontrol "teste" run --exe "/bin/ls" --username kali --password kali --wait-stdout

VBoxManage --nologo guestcontrol "teste" run --exe "script.sh" --username kali --password kali --wait-stdout

VBoxManage guestcontrol "teste" --username kali --password kali mkdir /home/kali/xyz

VBoxManage guestcontrol "teste" --username kali --password kali copyto script.sh

PasswordAuthentication no

sudo systemctl enable ssh.service

VBoxManage --nologo guestcontrol "teste" run --exe "sudo systemctl enable ssh.service" --username kali --password kali --wait-stdout

VBoxManage guestcontrol "teste" --username kali --password kali copyto script.sh /home/kali
VBoxManage --nologo guestcontrol "teste" run --exe "/home/kali/script.sh" --username kali --password kali --wait-stdout

ssh-copy-id -i $HOME/.ssh/id_ed25519.pub vagrant@192.168.15.64

VBoxManage guestcontrol "node1" --username vagrant --password vagrant copyto $HOME/.ssh/id_ed25519.pub /home/vagrant