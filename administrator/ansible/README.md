Configuracao Ansible VPS
=========

Configuracao base da VPS

Requirements
------------

Instalar ansible no cliente

```
setup_ansible_arch_client.py
```

Executando playbook
--------------

Exemplo inventory.ini

```
[myhosts]
192.168.0.50

[vps]
100.100.100.100
```

Commando de execucao playbook

```
ansible-playbook -i inventory.ini playbook.yml -u root --ask-pass

```

Dependencies
------------

NA

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).