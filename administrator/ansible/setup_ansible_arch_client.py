# Atualizacao do Sistema
sudo pacman -Syyu
sudo pacman -S sshpass

# Instalacao do python
sudo pacman -S python python-pip python-setuptools

# Criando virtual env
python3 -m venv venv
# Ativando virtual env
source venv/bin/activate

# Instalando ansible
pip install ansible
pip install passlib