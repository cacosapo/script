echo '###########################################################################'
echo 'apt update' 
echo '###########################################################################'
sudo apt update
sudo apt install build-essential automake autoconf libreadline-dev libncurses-dev libssl-dev libyaml-dev libxslt-dev \
libffi-dev libtool unixodbc-dev unzip curl zlib1g-dev sqlite3 libsqlite3-dev -y

echo '###########################################################################'
echo 'installing curl/tmux/ranger/ncdu/git/vim/python' 
echo '###########################################################################'
sudo apt install curl ranger ncdu git vim python3 python3-venv python3-pip -y

echo '###########################################################################'
echo 'installing docker' 
echo '###########################################################################'
sudo apt remove docker docker-engine docker.io
sudo apt install containerd.io docker-compose-plugin -y
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker --now
sudo usermod -aG docker $USER
docker --version

echo '###########################################################################'
echo 'installing minikube' 
echo '###########################################################################'
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
sudo install minikube-linux-arm64 /usr/local/bin/minikube
alias kubectl="minikube kubectl --"

echo '###########################################################################'
echo 'installing fastapi / django' 
echo '###########################################################################'
read -p "install fastapi / django? (y/n) " RESP
if [ "$RESP" = "y" ]; then
  pip install fastapi[all]
  pip install django
fi

echo '###########################################################################'
echo 'installing asdf' 
echo '###########################################################################'
read -p "install asdf? (y/n) " RESP
if [ "$RESP" = "y" ]; then
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.1
    cp ~/.bashrc ~/.bashrc.bkp
    echo '# ASDF Config' >> ~/.bashrc
    echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc
    echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
fi

echo '###########################################################################'
echo 'installing VS Code / Flutter / Intellij / Android-studio / Brave' 
echo '###########################################################################'
read -p "install VS Code / Flutter / Intellij / Android-studio / Brave? (y/n) " RESP
if [ "$RESP" = "y" ]; then
    sudo snap install code --classic
    sudo snap install intellij-idea-community --classic
    sudo snap install android-studio --classic
    sudo snap install brave --classic
fi

echo '###########################################################################'
echo 'Donwload/Installing Anaconda' 
echo '###########################################################################'
read -p "Donwload/Install Anaconda? (y/n) " RESP
if [ "$RESP" = "y" ]; then
    wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
    mkdir ~/.kaggle
fi
echo '###########################################################################'
echo 'installing Flutter' 
echo '###########################################################################'
read -p "Installing Flutter ? (y/n) " RESP
if [ "$RESP" = "y" ]; then
    sudo snap install flutter --classic
fi

############################################################################'
# SandBox
############################################################################'

#echo "What name do you want to use in GIT user.name?"
#echo "For example, mine will be \"Erick Wendel\""
#read git_config_user_name
#git config --global user.name "$git_config_user_name"
#clear 

#echo "What email do you want to use in GIT user.email?"
#echo "For example, mine will be \"erick.workspace@gmail.com\""
#read git_config_user_email
#git config --global user.email $git_config_user_email
#clear

#cd /opt
#sudo wget https://www.python.org/ftp/python/3.9.2/Python-3.9.2.tgz
#sudo tar xzf Python-3.9.2.tgz
#cd Python-3.9.2

#echo '###########################################################################'
#echo 'installing extensions'
#echo '###########################################################################'
#code --install-extension ms-python.python
#code --install-extension ms-python.vscode-pylance
#code --install-extension ms-toolsai.jupyter
#code --install-extension ms-toolsai.jupyter-keymap
#code --install-extension ms-toolsai.jupyter-renderers
#code --install-extension Dart-Code.dart-code
#code --install-extension Dart-Code.flutter
#code --install-extension dracula-theme.theme-dracula
#code --install-extension dbaeumer.vscode-eslint
#code --install-extension christian-kohler.path-intellisense
#code --install-extension dbaeumer.vscode-eslint
#code --install-extension dracula-theme.theme-dracula
#code --install-extension esbenp.prettier-vscode
#code --install-extension foxundermoon.shell-format
#code --install-extension pmneo.tsimporter
#code --install-extension waderyan.gitblame
#code --install-extension yzhang.markdown-all-in-one

#echo '###########################################################################'
#echo 'installing flat-remix'
#echo '###########################################################################'
#mkdir ~/Documentos/workspace/
#cd ~/Documentos/workspace/
#git clone https://github.com/daniruiz/flat-remix
#git clone https://github.com/daniruiz/flat-remix-gtk
#mkdir -p ~/.icons && mkdir -p ~/.themes
#cp -r flat-remix/Flat-Remix* ~/.icons/ && cp -r flat-remix-gtk/Flat-Remix-GTK* ~/.themes/

#gsettings set org.gnome.desktop.interface gtk-theme "Flat-Remix-GTK-Blue-Darkest-NoBorder"
#gsettings set org.gnome.desktop.interface icon-theme 'Flat-Remix-Blue-Dark'
#gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/matt-mcnulty-nyc-2nd-ave.jpg'
#gsettings set org.gnome.shell favorite-apps "['ubiquity.desktop', 'firefox.desktop','org.gnome.Nautilus.desktop']"
#dconf reset -f /org/gnome/
#gsettings set org.gnome.desktop.interface monospace-font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.interface document-font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.interface font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Hack Bold 13'
# gsettings reset-recursively org.gnome.desktop.interface
# gsettings list-recursively org.gnome.desktop.interface
# gsettings reset org.gnome.desktop.interface font-name
# gsettings reset org.gnome.desktop.interface document-font-name
# gsettings reset org.gnome.desktop.interface monospace-font-name
# gsettings reset org.gnome.desktop.wm.preferences titlebar-font
# gsettings reset org.gnome.desktop.interface text-scaling-factor
#gsettings set org.gnome.desktop.interface monospace-font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.interface document-font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.interface font-name 'Hack Regular 11'
#gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Hack Bold 13'

#gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'brave_brave.desktop']"

#cd ~
#mkdir projects
#cd projects
#git clone https://github.com/hut/ranger.git

# echo "What name do you want to use in GIT user.name?"
# echo "For example, mine will be \"Erick Wendel\""
# read git_config_user_name
# git config --global user.name "$git_config_user_name"
# clear 

# echo "What email do you want to use in GIT user.email?"
# echo "For example, mine will be \"erick.workspace@gmail.com\""
# read git_config_user_email
# git config --global user.email $git_config_user_email
# clear

# echo "Can I set VIM as your default GIT editor for you? (y/n)"
# read git_core_editor_to_vim
# if echo "$git_core_editor_to_vim" | grep -iq "^y" ;then
# 	git config --global core.editor vim
# else
# 	echo "Okay, no problem. :) Let's move on!"
# fi

# echo "Generating a SSH Key"
# ssh-keygen -t rsa -b 4096 -C $git_config_user_email
# ssh-add ~/.ssh/id_rsa
# cat ~/.ssh/id_rsa.pub | xclip -selection clipboard

# echo 'enabling workspaces for both screens' 
# gsettings set org.gnome.mutter workspaces-only-on-primary false

# echo 'installing zsh'
# sudo apt-get install zsh -y
# sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
# chsh -s /bin/zsh

# echo 'installing tool to handle clipboard via CLI'
# sudo apt-get install xclip -y

# export alias pbcopy='xclip -selection clipboard'
# export alias pbpaste='xclip -selection clipboard -o'
# source ~/.zshrc

# echo 'installing code'
# curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
# sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
# sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
# sudo apt-get install apt-transport-https -y
# sudo apt-get update
# sudo apt-get install code -y # or code-insiders

# echo 'installing extensions'
# code --install-extension dbaeumer.vscode-eslint
# code --install-extension christian-kohler.path-intellisense
# code --install-extension dbaeumer.vscode-eslint
# code --install-extension dracula-theme.theme-dracula
# code --install-extension esbenp.prettier-vscode
# code --install-extension foxundermoon.shell-format
# code --install-extension pmneo.tsimporter
# code --install-extension waderyan.gitblame
# code --install-extension yzhang.markdown-all-in-one

# echo 'installing spotify' 
# snap install spotify

# echo 'installing chrome' 
# wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
# sudo dpkg -i google-chrome-stable_current_amd64.deb

# echo 'installing nvm' 
# sh -c "$(curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash)"

# export NVM_DIR="$HOME/.nvm" && (
# git clone https://github.com/creationix/nvm.git "$NVM_DIR"
# cd "$NVM_DIR"
# git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
# ) && \. "$NVM_DIR/nvm.sh"

# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# source ~/.zshrc
# nvm --version
# nvm install 12
# nvm alias default 12
# node --version
# npm --version

# echo 'installing autosuggestions' 
# git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
# echo "source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc
# source ~/.zshrc

# echo 'installing theme'
# sudo apt install fonts-firacode -y
# wget -O ~/.oh-my-zsh/themes/node.zsh-theme https://raw.githubusercontent.com/skuridin/oh-my-zsh-node-theme/master/node.zsh-theme 
# sed -i 's/.*ZSH_THEME=.*/ZSH_THEME="node"/g' ~/.zshrc

# echo 'installing meet franz' 
# wget https://github.com/meetfranz/franz/releases/download/v5.1.0/franz_5.1.0_amd64.deb -O franz.deb
# sudo dpkg -i franz.debchristian-kohler.path-intellisense
# sudo apt-get install -y -f 

# echo 'installing slack' 
# wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.3.8-amd64.deb
# sudo apt install ./slack-desktop-*.deb -y

# echo 'installing terminator'
# sudo apt-get update
# sudo apt-get install terminator -y

#sudo add-apt-repository ppa:gnome-terminator
#sudo apt-get update
#sudo apt-get install terminator -y

# echo 'adding dracula theme' 
# cat <<EOF >  ~/.config/terminator/config
# [global_config]
#   title_transmit_bg_color = "#ad7fa8"
# [keybindings]
#   close_term = <Primary>w
#   close_window = <Primary>q
#   new_tab = <Primary>t
#   new_window = <Primary>i
#   paste = <Primary>v
#   split_horiz = <Primary>e
#   split_vert = <Primary>d
#   switch_to_tab_1 = <Primary>1
#   switch_to_tab_10 = <Primary>0
#   switch_to_tab_2 = <Primary>2
#   switch_to_tab_3 = <Primary>3
#   switch_to_tab_4 = <Primary>4
#   switch_to_tab_5 = <Primary>5
#   switch_to_tab_6 = <Primary>6
# [layouts]
#   [[default]]
#     [[[child1]]]
#       parent = window0
#       type = Terminal
#     [[[window0]]]
#       parent = ""
#       type = Window
# [plugins]
# [profiles]
#   [[default]]
#     cursor_color = "#aaaaaa"
# EOF

# cat <<EOF >>  ~/.config/terminator/config
# [[Dracula]]
#     background_color = "#1e1f29"
#     background_darkness = 0.88
#     background_type = transparent
#     copy_on_selection = True
#     cursor_color = "#bbbbbb"
#     foreground_color = "#f8f8f2"
#     palette = "#000000:#ff5555:#50fa7b:#f1fa8c:#bd93f9:#ff79c6:#8be9fd:#bbbbbb:#555555:#ff5555:#50fa7b:#f1fa8c:#bd93f9:#ff79c6:#8be9fd:#ffffff"
#     scrollback_infinite = True
# EOF

# chmod 777 /var/run/docker.sock
# docker run hello-world

# echo 'installing docker-compose' 
# sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose
# docker-compose --version

# echo 'installing aws-cli' 
# sudo apt-get install awscli -y
# aws --version
# curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
# sudo dpkg -i session-manager-plugin.deb
# session-manager-plugin --version

# echo 'installing teamviewer'
# wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
# sudo apt install -y ./teamviewer_amd64.deb

# echo 'installing vnc-viewer'
# sudo apt-get install -y --no-install-recommends ubuntu-desktop gnome-panel gnome-settings-daemon metacity nautilus gnome-terminal
# sudo apt-get install vnc4server -y 

# echo 'installing fzf'
# git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
# ~/.fzf/install --all

#sudo add-apt-repository --remove

#echo 'installing brave'
#curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
#source /etc/os-release
#echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/brave-browser-release-${UBUNTU_CODENAME}.list
#sudo apt update
#sudo apt install brave-keyring brave-browser

# echo 'installing dbeaver'
# wget -c https://dbeaver.io/files/6.0.0/dbeaver-ce_6.0.0_amd64.deb
# sudo dpkg -i dbeaver-ce_6.0.0_amd64.deb
# sudo apt-get install -f