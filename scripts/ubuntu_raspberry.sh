echo '###########################################################################'
echo 'apt update' 
echo '###########################################################################'
sudo apt update

echo '###########################################################################'
echo 'installing curl' 
echo '###########################################################################'
sudo apt install curl -y

echo '###########################################################################'
echo 'installing git' 
echo '###########################################################################'
sudo apt install git -y

echo '###########################################################################'
echo 'installing vim'
echo '###########################################################################'
sudo apt install vim -y

echo '###########################################################################'
echo 'installing lm-sensors'
echo '###########################################################################'
sudo apt install lm-sensors -y

echo '###########################################################################'
echo 'installing tmux'
echo '###########################################################################'
sudo apt install tmux -y

echo '###########################################################################'
echo 'installing ncdu'
echo '###########################################################################'
sudo apt install ncdu -y

echo '###########################################################################'
echo 'installing docker' 
echo '###########################################################################'
sudo apt remove docker docker-engine docker.io
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
docker --version

sudo groupadd docker
sudo usermod -aG docker ${USER}

echo '###########################################################################'
echo 'installing kubectl' 
echo '###########################################################################'
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

#echo '###########################################################################'
#echo 'installing microk8s' 
#echo '###########################################################################'
#sudo snap install microk8s --classic
#sudo usermod -a -G microk8s ubuntu
#sudo chown -f -R ubuntu ~/.kube
#microk8s enable dashboard dns registry istio

echo '###########################################################################'
echo 'installing minikube' 
echo '###########################################################################'
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
sudo install minikube-linux-arm64 /usr/local/bin/minikube && rm minikube-linux-arm64

