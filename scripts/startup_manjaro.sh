sudo pacman -S snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap

sudo snap install code --classic
sudo snap install intellij-idea-community --classic

sudo pamac install docker
sudo pacman -S docker-compose
sudo systemctl start docker.service
sudo systemctl enable docker.service

sudo groupadd docker
sudo usermod -aG docker $USER

code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-python.python